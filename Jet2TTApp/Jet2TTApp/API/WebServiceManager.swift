//
//  WebServiceManager.swift
//  Jet2TTApp
//
//  Created by Apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

final class WebServiceManager {
    
    private let webService: WebService!
    
    init(service: WebService) {
        self.webService = service
    }
    
    func getData(url: String, _ completion: @escaping ((Result<[Article]>) -> Void)){
        
        webService.fetchData(api: url) { (result) in
            
            switch result {
                
            case .success(let data):
                do {
                    
                    let response = try JSONDecoder().decode([Article].self, from: data)
                    
                    completion(.success(response))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
}
