//
//  WebService.swift
//  Jet2TTApp
//
//  Created by Apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import Alamofire

final class WebService {
    
    func fetchData(api: String, completion :@escaping ((Result<Data>) -> Void)) {
        
        let url: URL = URL(string: api)!
        var request = URLRequest(url: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = error {
                completion(.failure(error))
                return
            }
            completion(.success(data))
        }
        task.resume()
        
    }
    
}
