//
//  ArticleCell.swift
//  Jet2TTApp
//
//  Created by Apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleCell: UITableViewCell {

    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var imgArticle: UIImageView!
    @IBOutlet weak var lblArticleContent: UILabel!
    @IBOutlet weak var lblArticleTitle: UILabel!
    @IBOutlet weak var lblArticleURL: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    
    @IBOutlet weak var con_imgHgt: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var articleViewModel: ArticleViewModel? {
        
        didSet {
            guard let data = articleViewModel else {return}
          
            lblUsername.text = data.getUserName()
            lblDesignation.text = data.getUserDesignation()
            let userImgURL = URL(string: data.getUserImg())
            imgUser?.sd_setImage(with: userImgURL)
            
            
            lblTime.text = data.getTime()
            
            lblArticleTitle.text = data.getArticleTitle()
            lblArticleContent.text = data.getContent()
            lblArticleURL.text = data.getArticleURL()
            lblLikes.text = data.getLikes()
            lblComments.text = data.getComments()
            
            self.con_imgHgt.constant = 100
            let articleImgURL = URL(string: data.getArticleImg())
            if let imgURL = articleImgURL {
                imgArticle.sd_setImage(with: imgURL) { (img, err, imgCacheType, url) in
                    if err != nil {
                        self.con_imgHgt.constant = 0
                    }
                }
            } else {
                self.con_imgHgt.constant = 0
            }
        }
    }
}
