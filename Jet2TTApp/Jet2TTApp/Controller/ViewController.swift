//
//  ViewController.swift
//  Jet2TTApp
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    var articleArr:[ArticleViewModel] = []
    let webServiceManager = WebServiceManager(service: WebService())
    var webServiceURL: String!
    var page: Int = 1
    var loading: Bool = false
    var loadMore: Bool = true
    
    @IBOutlet weak var articleTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // add observer for network change
        NotificationCenter.default.addObserver(self, selector: #selector(handleNetworkStatus), name: NSNotification.Name(rawValue: NetworkStatusChanged), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // make API call
        self.showActivityIndicator()
        callAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleNetworkStatus() {
        
        self.alert(strTitle: alertTitle, strMsg: networkErrMSg)
    }
    
    func setupUI(){
        
        articleTableView.estimatedRowHeight = 300
        articleTableView.rowHeight = UITableView.automaticDimension
        articleTableView.separatorColor = .black
        articleTableView.separatorStyle = .singleLine
    }

    
    // MARK: API call method
    
    func callAPI() {
        
        webServiceURL = AppURL + "page=\(page)&limit=10"
        
        if Connectivity.isConnectedToInternet(){
            
            if !self.loading {
                
                self.loading = true
                
                
                webServiceManager.getData(url: webServiceURL) { (result) in
                    
                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                    }
                    
                    switch result {
                        
                    case .success(let data):
                        
                        if self.loading && data.count == 0 {
                            self.loadMore = false
                            self.loading = false
                        }
                        self.parseArticleData(data: data)
                        
                        DispatchQueue.main.async {
                            
                            self.articleTableView.reloadData()
                            self.loading = false
                        }
                        
                        
                    case .failure(let error):
                        
                        self.loading = false
                        DispatchQueue.main.async {
                            self.alert(strTitle: alertTitle, strMsg: error.localizedDescription)
                        }
                    }
                }
            }
        } else {
            
            self.alert(strTitle: alertTitle, strMsg: networkErrMSg)
        }
    }
    
    func parseArticleData(data: [Article]){
        
        if data.count > 0 {
            
            for article in data {
                
                self.articleArr.append(ArticleViewModel(data: article))
            }
            
        }
    }

    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.articleArr.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ArticleCell
            cell.articleViewModel = articleArr[indexPath.row]
            
            cell.layoutIfNeeded()
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellId, for: indexPath) as! LoadingCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }
        return loadMore && articleArr.count > 0 ? 40:0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.selectionStyle = .none
       
    }
    
    
}
extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            
            if loadMore && !loading{
                
                page = page + 1
                
                callAPI()
            }
            
        }
    }
}
