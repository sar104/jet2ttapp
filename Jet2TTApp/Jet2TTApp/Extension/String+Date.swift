//
//  String+Date.swift
//  Jet2TTApp
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

extension String {
    
    func stringToDate() -> Date {
        
        if self.isEmpty {
            return Date()
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
        
        return dateFormatterGet.date(from: self)!
        
    }
}
