//
//  User.swift
//  Jet2TTApp
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct User: Decodable {
   
    let id: String?
    let blogId: String?
    let createdAt: String?
    let name: String?
    let avatar: String?
    let lastname: String?
    let city: String?
    let designation: String?
    let about: String?
    
}
