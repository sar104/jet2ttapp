//
//  Article.swift
//  Jet2TTApp
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct Article: Decodable {
    
    let id: String?
    let createdAt: String?
    let content: String?
    let comments: Int?
    let likes: Int?
    let media: [Media]?
    let user: [User]?
    
}
