//
//  Media.swift
//  Jet2TTApp
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct Media: Decodable {
    
    let id: String?
    let blogId: String?
    let createdAt:String?
    let image: String?
    let title: String?
    let url: String?
}
