//
//  Connectivity.swift
//  Jet2TTApp
//
//  Created by Apple on 5/7/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    
    class func isConnectedToInternet() -> Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }
    
}
