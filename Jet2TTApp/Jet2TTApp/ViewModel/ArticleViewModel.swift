//
//  ArticleViewModel.swift
//  Jet2TTApp
//
//  Created by Apple on 5/6/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

class ArticleViewModel {
    
    var articleData: Article?
    
    init(data: Article) {
        self.articleData = data
    }
    
    func getUserName() -> String {
        
        if let user = self.articleData?.user, user.count > 0 {
            let name = (user[0].name ?? "") + " " + (user[0].lastname ?? "")
            return name
        }
        return ""
    }
    
    func getUserDesignation() -> String {
        
        if let user = self.articleData?.user, user.count > 0 {
            let designation = user[0].designation ?? ""
            return designation
        }
        return ""
    }
    
    func getUserImg() -> String {
        
        if let user = self.articleData?.user, user.count > 0 {
            let avatar = user[0].avatar ?? ""
            return avatar
        }
        return ""
    }
    
    func getTime() -> String {
        
        let date = self.articleData?.createdAt?.stringToDate()
        
        return (date?.timeAgoDisplay())!
 
    }
    
    func getContent() -> String {
        return self.articleData?.content ?? ""
    }
    
    func getArticleImg() -> String {
        if let media = self.articleData?.media, media.count > 0 {
            let img = media[0].image ?? ""
            return img
        }
        return ""
    }
    
    func getArticleTitle() -> String {
        if let media = self.articleData?.media, media.count > 0 {
            let title = media[0].title ?? ""
            return title
        }
        return ""
    }
    
    func getArticleURL() -> String {
        if let media = self.articleData?.media, media.count > 0 {
            let url = media[0].url ?? ""
            return url
        }
        return ""
    }
    
    func getLikes() -> String {
        
        if let likes = self.articleData?.likes {
            if likes >= 1000 {
                let strLikes = "\(likes/1000)K Likes"
                return strLikes
            } else {
                return "\(likes) Likes"
            }
        }
        return ""
    }
    
    func getComments() -> String {
        
        if let comments = self.articleData?.comments {
            if comments >= 1000 {
                let strComments = "\(comments/1000)K Comments"
                return strComments
            }else {
                return "\(comments) Comments"
            }
        }
        return ""
    }
}
